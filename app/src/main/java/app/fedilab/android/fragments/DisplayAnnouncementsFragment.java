package app.fedilab.android.fragments;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import app.fedilab.android.R;
import app.fedilab.android.asynctasks.RetrieveFeedsAsyncTask;
import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.Entities.Announcement;
import app.fedilab.android.interfaces.OnRetrieveFeedsInterface;
import es.dmoral.toasty.Toasty;


/**
 * Created by Thomas on 08/03/2020.
 * Fragment to display announcements
 */
public class DisplayAnnouncementsFragment extends Fragment implements OnRetrieveFeedsInterface {


    private Context context;
    private RelativeLayout textviewNoAction;
    private RelativeLayout mainLoader;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_announcements, container, false);
        context = getContext();
        final RecyclerView lv_annoucements = rootView.findViewById(R.id.lv_annoucements);
        mainLoader = rootView.findViewById(R.id.loader);
        textviewNoAction = rootView.findViewById(R.id.no_action);
        mainLoader.setVisibility(View.VISIBLE);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        lv_annoucements.setLayoutManager(mLayoutManager);
        new RetrieveFeedsAsyncTask(context, RetrieveFeedsAsyncTask.Type.ANNOUNCEMENTS, null, DisplayAnnouncementsFragment.this);
        return rootView;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCreate(Bundle saveInstance) {
        super.onCreate(saveInstance);
    }


    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public void onRetrieveFeeds(APIResponse apiResponse) {
        mainLoader.setVisibility(View.GONE);
        if (apiResponse == null) {
            Toasty.error(context, context.getString(R.string.toast_error), Toast.LENGTH_LONG).show();
            return;
        } else {
            if (apiResponse.getError() != null) {
                if (apiResponse.getError().getError().length() < 100) {
                    Toasty.error(context, apiResponse.getError().getError(), Toast.LENGTH_LONG).show();
                } else {
                    Toasty.error(context, getString(R.string.long_api_error, "\ud83d\ude05"), Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
        List<Announcement> announcements = apiResponse.getAnnouncements();
        if (announcements == null || announcements.size() == 0) {
            textviewNoAction.setVisibility(View.VISIBLE);
        }

    }
}
