/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.client.Entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Thomas on 23/04/2017.
 * Manages Media
 */


public class Attachment implements Parcelable {

    public static final Creator<Attachment> CREATOR = new Creator<Attachment>() {
        @Override
        public Attachment createFromParcel(Parcel source) {
            return new Attachment(source);
        }

        @Override
        public Attachment[] newArray(int size) {
            return new Attachment[size];
        }
    };
    private String id;
    private transient int viewId;
    private String type;
    private String mime;
    private String filter_class;
    private String filter_name;
    private String license;
    private String url;
    private String remote_url;
    private String preview_url;
    private String meta;
    private String text_url;
    private String description;
    private String local_path;
    private String orientation;
    private boolean is_nsfw = false;

    public Attachment() {

    }

    protected Attachment(Parcel in) {
        this.id = in.readString();
        this.viewId = in.readInt();
        this.type = in.readString();
        this.mime = in.readString();
        this.filter_class = in.readString();
        this.filter_name = in.readString();
        this.license = in.readString();
        this.url = in.readString();
        this.remote_url = in.readString();
        this.preview_url = in.readString();
        this.meta = in.readString();
        this.text_url = in.readString();
        this.description = in.readString();
        this.local_path = in.readString();
        this.orientation = in.readString();
        this.is_nsfw = in.readByte() != 0;
    }

    public int getViewId() {
        return viewId;
    }

    public void setViewId(int viewId) {
        this.viewId = viewId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRemote_url() {
        return remote_url;
    }

    public void setRemote_url(String remote_url) {
        this.remote_url = remote_url;
    }

    public String getPreview_url() {
        return preview_url;
    }

    public void setPreview_url(String preview_url) {
        this.preview_url = preview_url;
    }

    public String getText_url() {
        return text_url;
    }

    public void setText_url(String text_url) {
        this.text_url = text_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getLocal_path() {
        return local_path;
    }

    public void setLocal_path(String local_path) {
        this.local_path = local_path;
    }

    public boolean isIs_nsfw() {
        return is_nsfw;
    }

    public void setIs_nsfw(boolean is_nsfw) {
        this.is_nsfw = is_nsfw;
    }

    public String getFilter_class() {
        return filter_class;
    }

    public void setFilter_class(String filter_class) {
        this.filter_class = filter_class;
    }

    public String getFilter_name() {
        return filter_name;
    }

    public void setFilter_name(String filter_name) {
        this.filter_name = filter_name;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeInt(this.viewId);
        dest.writeString(this.type);
        dest.writeString(this.mime);
        dest.writeString(this.filter_class);
        dest.writeString(this.filter_name);
        dest.writeString(this.license);
        dest.writeString(this.url);
        dest.writeString(this.remote_url);
        dest.writeString(this.preview_url);
        dest.writeString(this.meta);
        dest.writeString(this.text_url);
        dest.writeString(this.description);
        dest.writeString(this.local_path);
        dest.writeString(this.orientation);
        dest.writeByte(this.is_nsfw ? (byte) 1 : (byte) 0);
    }
}
