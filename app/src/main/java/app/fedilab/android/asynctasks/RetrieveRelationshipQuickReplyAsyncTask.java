/* Copyright 2019 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

import app.fedilab.android.activities.MainActivity;
import app.fedilab.android.client.API;
import app.fedilab.android.client.Entities.Error;
import app.fedilab.android.client.Entities.Relationship;
import app.fedilab.android.interfaces.OnRetrieveRelationshipQuickReplyInterface;

/**
 * Created by Thomas on 29/06/2019.
 * Retrieves relationship between the authenticated user and another account
 */

public class RetrieveRelationshipQuickReplyAsyncTask {


    private final app.fedilab.android.client.Entities.Status status;
    private final OnRetrieveRelationshipQuickReplyInterface listener;
    private final WeakReference<Context> contextReference;
    private Relationship relationship;
    private Error error;

    public RetrieveRelationshipQuickReplyAsyncTask(Context context, app.fedilab.android.client.Entities.Status status, OnRetrieveRelationshipQuickReplyInterface onRetrieveRelationshipQuickReplyInterface) {
        this.contextReference = new WeakReference<>(context);
        this.listener = onRetrieveRelationshipQuickReplyInterface;
        this.status = status;
        doInBackground();
    }

    protected void doInBackground() {
        new Thread(() -> {
            if (MainActivity.social == UpdateAccountInfoAsyncTask.SOCIAL.MASTODON || MainActivity.social == UpdateAccountInfoAsyncTask.SOCIAL.PLEROMA) {
                API api = new API(this.contextReference.get());
                relationship = api.getRelationship(status.getReblog() != null ? status.getReblog().getAccount().getId() : status.getAccount().getId());
                error = api.getError();
            }
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onRetrieveRelationshipQuickReply(relationship, status, error);
            mainHandler.post(myRunnable);
        }).start();
    }

}
