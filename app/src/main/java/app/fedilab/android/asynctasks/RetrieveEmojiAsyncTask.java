/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;
import java.util.List;

import app.fedilab.android.client.Entities.Emojis;
import app.fedilab.android.interfaces.OnRetrieveEmojiInterface;
import app.fedilab.android.sqlite.CustomEmojiDAO;
import app.fedilab.android.sqlite.Sqlite;


/**
 * Created by Thomas on 01/11/2017.
 * Retrieves emojis
 */

public class RetrieveEmojiAsyncTask {

    private final String shortcode;
    private final OnRetrieveEmojiInterface listener;
    private final WeakReference<Context> contextReference;
    private List<Emojis> emojis;

    public RetrieveEmojiAsyncTask(Context context, String shortcode, OnRetrieveEmojiInterface onRetrieveEmojiInterface) {
        this.contextReference = new WeakReference<>(context);
        this.shortcode = shortcode;
        this.listener = onRetrieveEmojiInterface;
        doInBackground();
    }


    protected void doInBackground() {

        new Thread(() -> {
            SQLiteDatabase db = Sqlite.getInstance(contextReference.get().getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
            emojis = new CustomEmojiDAO(contextReference.get(), db).getEmojiStartingBy(shortcode);
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onRetrieveSearchEmoji(emojis);
            mainHandler.post(myRunnable);
        }).start();
    }

}
