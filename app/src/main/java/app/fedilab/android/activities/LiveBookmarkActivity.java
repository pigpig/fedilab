/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.activities;

import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;
import com.bumptech.glide.util.FixedPreloadSizeProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.fedilab.android.R;
import app.fedilab.android.asynctasks.RetrieveFeedsAsyncTask;
import app.fedilab.android.asynctasks.UpdateAccountInfoAsyncTask;
import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.Entities.Account;
import app.fedilab.android.client.Entities.Attachment;
import app.fedilab.android.client.Entities.Status;
import app.fedilab.android.client.Entities.StatusDrawerParams;
import app.fedilab.android.databinding.ActivityLiveBookmarkBinding;
import app.fedilab.android.drawers.PixelfedListAdapter;
import app.fedilab.android.drawers.StatusListAdapter;
import app.fedilab.android.fragments.DisplayStatusFragment;
import app.fedilab.android.helper.Helper;
import app.fedilab.android.interfaces.OnRetrieveFeedsInterface;
import app.fedilab.android.sqlite.AccountDAO;
import app.fedilab.android.sqlite.Sqlite;
import es.dmoral.toasty.Toasty;


/**
 * Created by Thomas on 28/01/2021.
 * Bookmark activity that are directly taken from the server
 */

public class LiveBookmarkActivity extends BaseActivity implements OnRetrieveFeedsInterface {

    private List<Status> statuses;
    private StatusListAdapter statusListAdapter;
    private PixelfedListAdapter pixelfedListAdapter;
    private ImageView pp_actionBar;
    private ActivityLiveBookmarkBinding binding;
    private boolean isSwipped, flag_loading, firstLoad;
    private String max_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedpreferences = getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
        int theme = sharedpreferences.getInt(Helper.SET_THEME, Helper.THEME_DARK);
        switch (theme) {
            case Helper.THEME_LIGHT:
                setTheme(R.style.AppTheme_NoActionBar_Fedilab);
                break;
            case Helper.THEME_BLACK:
                setTheme(R.style.AppThemeBlack_NoActionBar);
                break;
            default:
                setTheme(R.style.AppThemeDark_NoActionBar);
        }

        isSwipped = false;
        max_id = null;
        flag_loading = true;
        firstLoad = true;

        binding = ActivityLiveBookmarkBinding.inflate(getLayoutInflater());
        View viewRoot = binding.getRoot();
        setContentView(viewRoot);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            View view = inflater.inflate(R.layout.simple_action_bar, new LinearLayout(LiveBookmarkActivity.this), false);
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(LiveBookmarkActivity.this, R.color.cyanea_primary));
            view.setBackground(new ColorDrawable(ContextCompat.getColor(LiveBookmarkActivity.this, R.color.cyanea_primary)));
            actionBar.setCustomView(view, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            TextView title = actionBar.getCustomView().findViewById(R.id.toolbar_title);
            pp_actionBar = actionBar.getCustomView().findViewById(R.id.pp_actionBar);
            title.setText(R.string.bookmarks);
            ImageView close_conversation = actionBar.getCustomView().findViewById(R.id.close_conversation);
            if (close_conversation != null) {
                close_conversation.setOnClickListener(v -> finish());
            }
        } else {
            setTitle(R.string.bookmarks);
        }

        SQLiteDatabase db = Sqlite.getInstance(getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
        String userId = sharedpreferences.getString(Helper.PREF_KEY_ID, null);
        String instance = sharedpreferences.getString(Helper.PREF_INSTANCE, null);
        Account account = new AccountDAO(LiveBookmarkActivity.this, db).getUniqAccount(userId, instance);

        Helper.loadGiF(LiveBookmarkActivity.this, account, pp_actionBar);

        binding.loader.setVisibility(View.VISIBLE);

        this.statuses = new ArrayList<>();

        boolean isOnWifi = Helper.isOnWIFI(LiveBookmarkActivity.this);
        StatusDrawerParams statusDrawerParams = new StatusDrawerParams();
        statusDrawerParams.setType(RetrieveFeedsAsyncTask.Type.BOOKMARKS);
        statusDrawerParams.setOnWifi(isOnWifi);
        statusDrawerParams.setStatuses(this.statuses);

        if (MainActivity.social != UpdateAccountInfoAsyncTask.SOCIAL.PIXELFED) {
            statusListAdapter = new StatusListAdapter(statusDrawerParams);
            binding.lvStatus.setAdapter(statusListAdapter);
        } else {
            pixelfedListAdapter = new PixelfedListAdapter(statusDrawerParams);
            binding.lvStatus.setAdapter(pixelfedListAdapter);
        }
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(LiveBookmarkActivity.this);
        binding.lvStatus.setLayoutManager(mLayoutManager);

        ListPreloader.PreloadSizeProvider sizeProvider =
                new FixedPreloadSizeProvider(640, 480);
        ListPreloader.PreloadModelProvider modelProvider = new MyPreloadModelProvider();
        RecyclerViewPreloader preloader =
                new RecyclerViewPreloader<>(Glide.with(LiveBookmarkActivity.this), modelProvider, sizeProvider, 20);
        binding.lvStatus.addOnScrollListener(preloader);

        binding.swipeContainer.setOnRefreshListener(() -> {
            if (statuses.size() > 0) {
                int size = statuses.size();
                isSwipped = true;
                statuses.clear();
                statuses = new ArrayList<>();
                max_id = null;
                if (statusListAdapter != null) {
                    statusListAdapter.notifyItemRangeRemoved(0, size);
                } else if (pixelfedListAdapter != null) {
                    pixelfedListAdapter.notifyItemRangeRemoved(0, size);
                }
                firstLoad = true;
                new RetrieveFeedsAsyncTask(LiveBookmarkActivity.this, RetrieveFeedsAsyncTask.Type.BOOKMARKS, null, LiveBookmarkActivity.this);
            }

        });

        binding.lvStatus.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    if (firstVisibleItem + visibleItemCount == totalItemCount) {
                        if (!flag_loading) {
                            flag_loading = true;
                            new RetrieveFeedsAsyncTask(LiveBookmarkActivity.this, RetrieveFeedsAsyncTask.Type.BOOKMARKS, max_id, LiveBookmarkActivity.this);
                            binding.loadingNextStatus.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.loadingNextStatus.setVisibility(View.GONE);
                    }
                }
            }
        });

        new RetrieveFeedsAsyncTask(LiveBookmarkActivity.this, RetrieveFeedsAsyncTask.Type.BOOKMARKS, null, LiveBookmarkActivity.this);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRetrieveFeeds(APIResponse apiResponse) {
        binding.loader.setVisibility(View.GONE);
        binding.loadingNextStatus.setVisibility(View.GONE);
        //handle other API error but discards 404 - error which can often happen due to toots which have been deleted
        if (this.statuses == null || apiResponse == null || (apiResponse.getError() != null && apiResponse.getError().getStatusCode() != 404 && apiResponse.getError().getStatusCode() != 501)) {
            if (apiResponse == null)
                Toasty.error(LiveBookmarkActivity.this, getString(R.string.toast_error), Toast.LENGTH_LONG).show();
            else {
                if (apiResponse.getError().getError().length() < 100) {
                    Toasty.error(LiveBookmarkActivity.this, apiResponse.getError().getError(), Toast.LENGTH_LONG).show();
                } else {
                    Toasty.error(LiveBookmarkActivity.this, getString(R.string.long_api_error, "\ud83d\ude05"), Toast.LENGTH_LONG).show();
                }
            }
            binding.swipeContainer.setRefreshing(false);
            flag_loading = false;
            return;
        }

        int previousPosition = this.statuses.size();
        List<Status> statuses;

        if (apiResponse.getResults() != null && apiResponse.getResults().getStatuses() != null)
            statuses = apiResponse.getResults().getStatuses();
        else
            statuses = apiResponse.getStatuses();

        //At this point all statuses are in "List<Status> statuses"
        max_id = apiResponse.getMax_id();

        //while max_id is different from null, there are some more toots to load when scrolling
        flag_loading = (max_id == null);
        //If it's the first load and the reply doesn't contain any toots, a message is displayed.
        if (firstLoad && (statuses == null || statuses.size() == 0)) {
            binding.noAction.setVisibility(View.VISIBLE);
            binding.lvStatus.setVisibility(View.GONE);
        } else {
            binding.lvStatus.setVisibility(View.VISIBLE);
            binding.noAction.setVisibility(View.GONE);
        }

        //Let's deal with statuses
        if (statuses != null && statuses.size() > 0) {
            this.statuses.addAll(statuses);
            if (!isSwipped) {
                if (statusListAdapter != null) {
                    statusListAdapter.notifyItemRangeInserted(previousPosition, statuses.size());
                } else if (pixelfedListAdapter != null) {

                    pixelfedListAdapter.notifyItemRangeInserted(previousPosition, statuses.size());
                }
            } else {
                isSwipped = false;
                boolean isOnWifi = Helper.isOnWIFI(LiveBookmarkActivity.this);
                StatusDrawerParams statusDrawerParams = new StatusDrawerParams();
                statusDrawerParams.setType(RetrieveFeedsAsyncTask.Type.BOOKMARKS);
                statusDrawerParams.setOnWifi(isOnWifi);
                statusDrawerParams.setStatuses(this.statuses);

                if (MainActivity.social != UpdateAccountInfoAsyncTask.SOCIAL.PIXELFED) {
                    statusListAdapter = new StatusListAdapter(statusDrawerParams);
                    binding.lvStatus.setAdapter(statusListAdapter);
                } else {
                    pixelfedListAdapter = new PixelfedListAdapter(statusDrawerParams);
                    binding.lvStatus.setAdapter(pixelfedListAdapter);
                }
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(LiveBookmarkActivity.this);
                binding.lvStatus.setLayoutManager(mLayoutManager);
            }
        }
        binding.swipeContainer.setRefreshing(false);
        firstLoad = false;

    }


    private class MyPreloadModelProvider implements ListPreloader.PreloadModelProvider<String> {
        @Override
        @NonNull
        public List<String> getPreloadItems(int position) {
            if (statuses == null || statuses.size() == 0) {
                return Collections.emptyList();
            }
            Status status = statuses.get(position);
            if (status.getMedia_attachments() == null || status.getMedia_attachments().size() == 0) {
                return Collections.emptyList();
            }
            List<String> preloaded_urls = new ArrayList<>();
            for (Attachment attachment : status.getMedia_attachments()) {
                preloaded_urls.add(attachment.getPreview_url());
            }
            return preloaded_urls;
        }

        @Nullable
        @Override
        public RequestBuilder<?> getPreloadRequestBuilder(@NonNull String url) {
            return Glide.with(LiveBookmarkActivity.this)
                    .load(url)
                    .thumbnail(0.1f)
                    .override(640, 480);
        }

    }
}
