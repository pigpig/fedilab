Added:
- 100 Mb of cache for videos (can be changed in settings)
- Bibliogram support (default: disabled)
- Ouiches support for audio

Changed:
- Closing media
- Smoother settings

Fixed:
- Peertube comments
- Some layout issues
- Some other crashes