Added:
-  Silesian localization

Changed:
- Add Silesian in language picker

Fixed:
- Some URLs not clickable
- Empty home timeline
- Some crashes when scrolling
- Question mark bug