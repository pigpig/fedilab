Added:
- Watermarks for pictures (default: disabled)
- Blur sensitive media for Pixelfed
- Put messages into drafts when replying to a deleted message
- Allow to schedule with another account

Fixed:
- Quick replies lose text when doing another action
- Fix refresh token (Peertube/Pixelfed)