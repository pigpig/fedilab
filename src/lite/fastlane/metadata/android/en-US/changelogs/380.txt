Added:
- notify when a followed user posts (subscribe by clicking the bell icon on profiles).

Fixed:
- Crash when importing media due to photo editor
- Some crashes when doing actions